package javaapplication1;
import java.util.Scanner;

public class JavaApplication1 {
    
    static int[][] matriz;
 
    public static void main(String[] args) {
        
        presentacion();
        menu();
        
    }
    
    static void presentacion(){
        System.out.println("Matemáticas Discretas.");
        System.out.println("Integrantes: ");
        System.out.println("Frank Esneider Sanabria Albarracín - 77372");
        System.out.println("Juan Pablo Franco Ramirez - 77725");
        System.out.println("Diego Armando Marín codigo - 77373");
    }
    
    // menu
    static void menu(){
        
        Scanner sc = new Scanner(System.in);        
        System.out.println("Seleccione el menú.");
        System.out.println("\n 1) opción random");
        System.out.println("\n 2) matriz manual");
        
        int nn = sc.nextInt();
        
        switch(nn){
        
            case 1:
                random();
            break;
            
            case 2:
                
                inicializar();
                visualizar();
                miRelacion();
                
                //convertir filas en columnas
                traspuesta();
                //convertir filas en columnas
                
                tipoMatriz();

            break;
        }
        
    }
    // menu
    
    // reflexiva
        static boolean esMatrizReflexiva(){
        int contador = 0;
        for(int i=0; i < matriz.length; i++){
            for(int j=0; j < matriz[i].length ; j++){
                if( ( i == j ) && matriz[i][j] == 1 ){
                    contador = contador + 1;
                }
            }
        }
        return (contador == matriz.length) ? true : false;
    }
    // reflexiva
    
    // irreflexiva
        static boolean esMatrizIrreflexiva(){
        int contador = 0;
        for(int i=0; i < matriz.length; i++){
            for(int j=0; j < matriz[i].length ; j++){
                if( ( i == j ) && matriz[i][j] == 0 ){
                    contador = contador + 1;
                }
            }
        }
        return (contador == matriz.length) ? true : false;
    }
    // irreflexiva
 
    // Simetrica
    static boolean esMatrizSimetrica(){
        for(int i=0; i < matriz.length; i++){
            for(int j=0; j < matriz[i].length ; j++){
                if(matriz[i][j] != matriz[j][i]){
                    return false;
                }
            }
        }
                
        return true;
    }
    // Simetrica
 
    static void inicializar(){
        int validar;
        Scanner sc = new Scanner(System.in);
        System.err.print(" \n Introduzca la dimensión de la matriz: \n - Esta sera para filas y columnas: ");
        int dim = sc.nextInt();
        matriz = new int[dim][dim];
        for(int i=0;i<matriz.length;i++){
               for(int j=0;j<matriz.length;j++){
               System.out.printf("Introduce el valor fila %d - columna %d: ", (i+1), (j+1));        
               validar = sc.nextInt();
               
                if(validar == 1 || validar == 0){
                    matriz[i][j] = validar;
                }else{
                    System.err.printf("Solo se recibe valores entre 0 y 1, el valor de fila: "+(i+1)+" - columna: "+(j+1)+" es de: 0 \n"); 
                    matriz[i][j] = 0;
                }
            }
        }
        
        sc.close();
    }
 
    static void miRelacion(){
        System.out.println("\n Mi relación con números 1: \n");
        for(int i=0;i<matriz.length;i++){
            for(int j=0;j<matriz.length;j++){
                if(matriz[i][j] == 1){
                    System.out.print("("+i+","+j+") \t");
                }
            }
            System.out.println();
        }
    }
    
    static void visualizar(){
        System.out.println("\n Matriz ingresada \n");
        for(int i=0;i<matriz.length;i++){
            for(int j=0;j<matriz[i].length;j++){
                System.out.print(matriz[i][j] + "\t");
            }
            System.out.println();
        }
    }
    
    static void traspuesta(){
        System.out.println("\n Matriz traspuesta \n");
        for(int i=0;i<matriz.length;i++){
            for(int j=0;j<matriz[i].length;j++){
                System.out.print(matriz[j][i] + "\t");
            }
            System.out.println();
        }
    }
    
    static void random(){
        
        int tamanoMatriz = (int) (Math.random()*(2-10)+10);
        int dim = tamanoMatriz;
        matriz = new int[dim][dim];
        for(int i=0;i<matriz.length;i++){
            for(int j=0;j<matriz[i].length;j++){
                int valor = (int) (Math.random()*(1-10)+10);
                matriz[i][j] = valor <= 6 ? 1 : 0; 
            }
        }
        
        visualizar();
        miRelacion();
        tipoMatriz();
    }
    
    static boolean asimetrica(){
        boolean resul = false;
        int valFila;
        int valColum;
        int valDiagonal;
        for(int i=0; i < matriz.length; i++){
            valDiagonal = matriz[i][i];
            if(valDiagonal == 0){
                for(int j=0; j < matriz.length ; j++){
                    if(i != j){
                        valFila = matriz[i][j];
                        valColum = matriz[j][i];
                        if((valFila == 0 && valColum == 1) || (valFila == 1 && valColum == 0)) {
                          resul = true;  
                        }else{
                          resul = false;
                          i = matriz.length + 1;
                          j = matriz.length + 1;
                        }
                    }
                }
            }
        }
        
        return resul;
    }
    
    static boolean antisimetrica(){
        boolean resul = false;
        int valFila = 0;
        int valColum = 0;
        for(int i=0; i < matriz.length; i++){
            for(int j=0; j < matriz.length ; j++){
                if(i != j){
                    valFila = matriz[i][j];
                    valColum = matriz[j][i];
                    if((valFila == 0 && valColum == 1) || (valFila == 1 && valColum == 0) || (valFila == 0 && valColum == 0)) {
                      resul = true;  
                    }else{
                      resul = false;
                      i = matriz.length + 1;
                      j = matriz.length + 1;
                    }
                }
            }
        }
        
        return resul;
    }
    
    static boolean transitiva(){
        boolean resul = false;
        int valFila;
        int valfColumfFila;
        for(int i=0; i < matriz.length; i++){
            for(int j=0; j < matriz.length; j++){
                valFila = matriz[i][j];
                //System.out.println("esto es val matriz "+ matriz[i][j]);
                if(valFila == 1){
                    for (int b = 0; b < matriz.length; b++) {
                        valfColumfFila = matriz[j][b];
                        if(valfColumfFila == 1){
                            resul = true;
                        }else{
                            resul = false;
                            b = matriz.length + 1;
                            j = matriz.length + 1;
                            i = matriz.length + 1;
                        }
                    }
                }
            }
        }
        
        return resul;
    }
    
    static void tipoMatriz(){
        
        //convertir filas en clumnas
        traspuesta();
        //convertir filas en clumnas
        
        // Simetrica
        System.out.printf("\n La matriz %s simétrica.", esMatrizSimetrica()?"es":"no es");
        // Simetrica
                
        // asimetrica
        System.out.printf("\n La matriz %s asimetrica. ", asimetrica()?"es":"no es");
        // asimetrica
                
        // antisimetrica
        System.out.printf("\n La matriz %s antisimetrica. ", antisimetrica()?"es":"no es");
        // antisimetrica

        // reflexiva
        System.out.printf("\n La matriz %s reflexiva. ", esMatrizReflexiva()?"es":"no es");
        // reflexiva
                
        // Irreflexiva
        System.out.printf("\n La matriz %s irreflexiva. ", esMatrizIrreflexiva()?"es":"no es");
        // Irreflexiva
                                
        // transitiva
        System.out.printf("\n La matriz %s transitiva. ", transitiva()?"es":"no es");
        // transitiva
        
    }
}